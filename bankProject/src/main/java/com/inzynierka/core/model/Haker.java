package com.inzynierka.core.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class Haker {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    String hakerName;

    boolean isProffesional=true;

    @OneToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    Set<PrimaryAccount> primaryAccounts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHakerName() {
        return hakerName;
    }

    public void setHakerName(String hakerName) {
        this.hakerName = hakerName;
    }

    public boolean isProffesional() {
        return isProffesional;
    }

    public void setProffesional(boolean proffesional) {
        isProffesional = proffesional;
    }

    public Set<PrimaryAccount> getPrimaryAccounts() {
        return primaryAccounts;
    }

    public void setPrimaryAccounts(Set<PrimaryAccount> primaryAccounts) {
        this.primaryAccounts = primaryAccounts;
    }

    public static final class HakerBuilder {
        Long id;
        String hakerName;
        boolean isProffesional=true;
        Set<PrimaryAccount> primaryAccounts;

        private HakerBuilder() {
        }

        public static HakerBuilder aHaker() {
            return new HakerBuilder();
        }

        public HakerBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public HakerBuilder withHakerName(String hakerName) {
            this.hakerName = hakerName;
            return this;
        }

        public HakerBuilder withIsProffesional(boolean isProffesional) {
            this.isProffesional = isProffesional;
            return this;
        }

        public HakerBuilder withPrimaryAccounts(Set<PrimaryAccount> primaryAccounts) {
            this.primaryAccounts = primaryAccounts;
            return this;
        }

        public Haker build() {
            Haker haker = new Haker();
            haker.setId(id);
            haker.setHakerName(hakerName);
            haker.setPrimaryAccounts(primaryAccounts);
            haker.isProffesional = this.isProffesional;
            return haker;
        }
    }
}
